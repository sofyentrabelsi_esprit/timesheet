package tn.esprit.spring.service;

 
import java.text.ParseException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Role;
import tn.esprit.spring.services.IEmployeService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeServiceImplTest {

		@Autowired
		IEmployeService es; 
	
//		@Test
//		public void testRetrieveAllEmployes() {
//			List<Employe> listEmployes = es.retrieveAllEmployes(); 
//			// if there are 7 users in DB : 
//			Assert.assertEquals(15, listEmployes.size());
//		}
		
		
		@Test
		public void testAddEmploye() throws ParseException {
			Employe e = new Employe("Mayssa1", "Mayssa1","personal_mail","pwd", true, Role.INGENIEUR); 
			Employe employeAdded = es.addEmploye(e); 
			Assert.assertEquals(e.getNom(), employeAdded.getNom());
		}
	 
		@Test
		public void testModifyEmploye() throws ParseException   {
			Employe e = new Employe(1L, "Mayssa122222222", "Mayssa","mail_pro", "new pwd",false, Role.INGENIEUR); 
			Employe employeUpdated  = es.updateEmploye(e); 
			Assert.assertEquals(e.getNom(), employeUpdated.getNom());
		}
	
		@Test
		public void testRetrieveEmploye() {
			Employe employeRetrieved = es.retrieveEmploye("1"); 
			Assert.assertEquals(1L, employeRetrieved.getId().longValue());
		}
		
//		@Test
//		public void testDeleteEmploye() {
//			es.deleteEmploye("3");
//			Assert.assertNull(es.retrieveEmploye("3"));
//		}
		
		// 5 tests unitaires  
 
}





