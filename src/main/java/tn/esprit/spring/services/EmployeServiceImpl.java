package tn.esprit.spring.services;

import java.util.List;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.repository.EmployeRepository;

@Service
public class EmployeServiceImpl implements IEmployeService {

	@Autowired
	EmployeRepository employeRepository;

	private static final Logger l = LogManager.getLogger(EmployeServiceImpl.class);



	
	
	@Override
	public List<Employe> retrieveAllEmployes() { 
		List<Employe> employes = null; 
		try {
	
			l.info("In retrieveAllEmployes() : ");
			employes = (List<Employe>) employeRepository.findAll();  
			for (Employe employe : employes) {
				l.debug("employe +++ : " + employe.toString());
			} 
			l.info("Out of retrieveAllEmployes() : ");
		}catch (Exception e) {
			l.error("Error in retrieveAllEmployes() : ".concat(e.toString()));
		}

		return employes;
	}


	@Override
	public Employe addEmploye(Employe e) {
		try {
			l.info("In addEmploye() : " + e);
			return employeRepository.save(e);
		}catch (Exception exc) {
			l.error("Error in addEmploye() : " + exc);
			return null;
		}
	}

	@Override 
	public Employe updateEmploye(Employe e) { 
		try {
			l.info("In updateEmploye() : " + e);
			return employeRepository.save(e);
		}catch (Exception exc) {
			l.error("Error in updateEmploye() : " + exc);
			return null;
		}
	}

	@Override
	public void deleteEmploye(String id) {
		try {
			l.info("In delteEmploye() : " + id);
			employeRepository.deleteById(Long.parseLong(id));
		}catch (Exception exc) {
			l.error("Error in deleteEmploye() : " + exc);
		}
	}

	@Override
	public Employe retrieveEmploye(String id) {
		l.info("in  retrieveEmploye id = " + id);
		//User u =  userRepository.findById(Long.parseLong(id)).orElse(null);
		//int i = 1/0; 
		Employe e =  employeRepository.findById(Long.parseLong(id)).get(); 
		l.info("employe returned : " + e);
		return e; 
	}

}
